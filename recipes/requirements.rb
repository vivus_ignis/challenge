include_recipe 'ohai'
include_recipe 'route53'

chef_gem 'aws-sdk' do
  version node['babbel-challenge']['aws_gem_version']
end

ohai 'reload' do
  action :reload
end

cookbook_file "#{node['ohai']['plugin_path']}/ec2_tags.rb" do
  source   'ec2_tags.rb'
  notifies :reload, "ohai[reload]", :immediately
end
