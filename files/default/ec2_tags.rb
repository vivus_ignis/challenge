require 'aws-sdk'

Ohai.plugin(:Ec2tags) do
  depends 'ec2'
  provides 'ec2tags'

  def region
    ec2['placement_availability_zone'][0..-2]
  end

  collect_data(:default) do
    ec2tags Mash.new

    ec2_api = Aws::EC2::Client.new(region: region)

    ec2_api.describe_tags({
      filters: [{
        name:   'resource-id',
        values: [ ec2['instance_id'] ]
      }]
    }).tags.each { |t|
      ec2tags[t.key] = t.value
    }

    Chef::Log.debug("ec2tags // #{ec2tags}")

    ec2tags
  end
end
