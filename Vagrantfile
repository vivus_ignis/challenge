# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = '2'

Vagrant.require_version '>= 1.5.0'

$enable_ec2_ohai_plugin = <<END
  [ -d /etc/chef/ohai/hints ] || mkdir -p /etc/chef/ohai/hints
  [ -f /etc/chef/ohai/hints/ec2.json ] || touch /etc/chef/ohai/hints/ec2.json
END

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.hostname = 'babbel-challenge-berkshelf'

  if Vagrant.has_plugin?("vagrant-omnibus")
    config.omnibus.chef_version = 'latest'
  end

  config.vm.box = 'dummy'

  config.vm.provider :aws do |aws, override|
    aws.access_key_id            = ENV['AWS_ACCESS_KEY_ID']
    aws.secret_access_key        = ENV['AWS_SECRET_ACCESS_KEY']
    aws.region                   = ENV['AWS_DEFAULT_REGION']
    aws.keypair_name             = ENV['AWS_KEYPAIR']
    aws.ami                      = 'ami-d05e75b8' # Ubuntu Server 14.04 LTS (HVM), SSD Volume Type
    aws.associate_public_ip      = true
    aws.instance_type            = 't2.micro'
    aws.subnet_id                = ENV['AWS_SUBNET_ID']
    aws.security_groups          = ENV['AWS_SECURITY_GROUPS'].split.to_a  # ssh, egress
    aws.iam_instance_profile_arn = ENV['AWS_IAM_PROFILE']
    aws.tags = {
      'cloudformation:stack-name' => 'bblchlng'
    }

    override.ssh.username         = 'ubuntu'
    override.ssh.private_key_path = "#{ENV['HOME']}/crypt/keys/us-east-1_baron_dl.pem"
  end

  config.berkshelf.enabled = true

  config.vm.provision :shell, inline: $enable_ec2_ohai_plugin
  config.vm.provision :chef_solo do |chef|
    chef.log_level = 'debug'
    chef.json = {
      'babbel-challenge' => {
        route53_zone_id: 'Z2BGJ66MYIIYRL',
        route53_zone_name: 'test.example.com',
        cf_stack_name_tag: 'cloudformation:stack-name' # aws: namespace is reserved
      }
    }

    chef.run_list = [
      'recipe[babbel-challenge::default]'
      # 'recipe[babbel-challenge::delete]'
    ]
  end
end
