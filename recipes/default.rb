#
# Cookbook Name:: babbel-challenge
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'babbel-challenge::requirements'
Chef::Resource.send(:include, Babbel_Challenge::Helper)

route53_record 'instance record' do
  name        lazy { aws_dns_name }
  value       node['ec2']['public_ipv4']
  type        'A'
  zone_id     node['babbel-challenge']['route53_zone_id']
  overwrite   true
  action      :create
end
