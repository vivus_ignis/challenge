module Babbel_Challenge
  module Helper

    def aws_dns_name
      h = if node.has_key? 'ec2tags' and
            node['ec2tags'].has_key? node['babbel-challenge']['cf_stack_name_tag']

            node['ec2']['instance_id'] + '.' +
            node['ec2tags'][ node['babbel-challenge']['cf_stack_name_tag'] ]
          else
            node['ec2']['instance_id']
          end
      "#{h}.#{node['babbel-challenge']['route53_zone_name']}"
    end

  end
end
