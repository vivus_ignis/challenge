include_recipe 'babbel-challenge::requirements'
Chef::Resource.send(:include, Babbel_Challenge::Helper)

route53_record 'instance record' do
  name    lazy { aws_dns_name }
  type    'A'
  zone_id node['babbel-challenge']['route53_zone_id']
  action  :delete
end
