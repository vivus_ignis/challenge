default:
	. .exports && vagrant rsync
	. .exports && vagrant provision

p:
	. .exports && vagrant provision

install:
	. .exports && vagrant up --provider=aws

clean:
	. .exports && vagrant destroy -f

.PHONY: default install p clean
